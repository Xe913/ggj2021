﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportArea : MonoBehaviour
{
    [SerializeField] private GameObject currentWorld;
    [SerializeField] private GameObject nextWorld;
    [SerializeField] private List<GameObject> teleportRenderers;

    private Camera playerCamera;

    void Start()
    {
        playerCamera = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Camera>();
    }

    void Update()
    {

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag != "Player") return;
        foreach (GameObject teleportRenderer in teleportRenderers)
        {
            if (CheckVisibility(teleportRenderer))
            {
                nextWorld.SetActive(true);
                currentWorld.SetActive(false);
                break;
            }
        }
    }

    private bool CheckVisibility(GameObject go)
    {
        //return go.GetComponent<Renderer>().isVisible;
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(playerCamera);
        if (GeometryUtility.TestPlanesAABB(planes, go.GetComponent<Collider>().bounds))
        {
            Mesh mesh = go.GetComponent<MeshFilter>().mesh;
            Vector3[] vertices = mesh.vertices;
            RaycastHit hit;
            foreach (Vector3 vertex in vertices)
            {
                Debug.DrawLine(go.transform.TransformPoint(vertex), playerCamera.transform.position, Color.red);
                Physics.Raycast(go.transform.TransformPoint(vertex), playerCamera.transform.position - go.transform.TransformPoint(vertex), out hit);
                if (hit.transform != null && hit.transform.gameObject.tag == "Player")
                {
                    return true;
                }
            }
        }
        return false;
    }
}
